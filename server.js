const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');

const app = express();

app.use(bodyParser.urlencoded());
app.set('view engine', 'ejs');

app.get('/api/todos', (req, res) => {
  db.allTodos()
    .then((todos) => {
      return res.json(todos);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});

app.get('/', (req, res) => {
  db.allTodos()
    .then((todos) => {
      console.log("second");
      return res.render('todos', {todos});
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("ERROR");
    });
  console.log("First");
});

app.post('/api/todos/:id/complete', (req, res) => {
  db.setCompleted(req.params.id)
    .then(() => {
      return res.redirect('/');
    })
    .catch((err) => {
      res.status(500).send("ERROR " + err);
    });
});

app.post('/api/todos', (req, res) => {
  const task = req.body.task;
  db.addTodo(task)
    .then(() => {
      return res.redirect('/');
    })
    .catch((err) => {
      res.status(400).send("ERROR " + err);
    });
});

db.connect()
  .then(() => {
    app.listen(3000);
    console.log("Listening on port 3000");
    return true;
  })
  .catch((err) => {
    console.log("Error establishing db connection", err);
  });
