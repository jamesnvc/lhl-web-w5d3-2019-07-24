const db = require('./db');

db.connect()
  .then(() => {
    console.log("Connected successfully");
    return db.initDb();
  })
  // .then(() => Promise.all([
    // db.addTodo("Some stuff"),
    // db.addTodo("Write lecture notes"),
    // db.addTodo("Eat lunch"),
    // db.addTodo("Go shopping")
  // ]))
  .then(() => db.addTodo("Some stuff"))
  .then((lastAdded) => {
    console.log("Last todo", lastAdded);
    return db.setCompleted(lastAdded.id);
  })
  .then((updateResult) => {
    console.log("updated", updateResult);
    return true;
  })
  // .then((results) => {
  //   console.log("Inserted", results);
  //   return true;
  // })
  .then(() => db.allTodos())
  .then((results) => {
    console.log("all todos", results);
    return true;
  })
  .catch((err) => {
    console.log("Error connecting to database", err);
    return false;
  })
  .finally(() => {
    db.close();
    console.log("done");
  });
