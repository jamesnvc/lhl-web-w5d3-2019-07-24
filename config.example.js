// rename this file to config.js & set parameters below appropriately
module.exports = {
  user: "tester", // vagrant
  password: "test", // vagrant
  database: "lhl_2019-07-24_101137", // whatever you want, but you'll need to create it
  host: "localhost",
  port: 5432 // default for postgres
};
