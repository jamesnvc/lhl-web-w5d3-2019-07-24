const pg = require('pg');
const config = require('./config');

const client = new pg.Client(config);

const connect = () => {
  return client.connect();
};

const close = () => {
  return client.end();
};

const initDb = () => {
  return client.query(
    `BEGIN; -- putting in a transaction
     -- so if we were creating multiple tables, either
     -- they would all succeed or none
     CREATE TABLE IF NOT EXISTS todos (
        id bigserial primary key,
        task text not null CHECK task_len_chk (char_length(task) > 0),
        created_at timestamp with time zone not null default now(),
        completed_at timestamp with time zone
     );
     COMMIT;`);
};

const addTodo = (task) => {
  return client.query(
    {text: `INSERT INTO todos (task) VALUES ($1) RETURNING *;`,
     values: [task],
     name: 'add_todo'}
  ).then((results) => results.rows[0]);
};

const allTodos = () => {
  return client.query(
    {text: `SELECT *, completed_at is not null as is_completed
            FROM todos ORDER BY created_at ASC;`,
     values: [],
     name: 'all_todos'}
  ).then((results) => results.rows);
};

const setCompleted = (id) => {
  return client.query(
    `UPDATE todos SET completed_at = now() WHERE id = $1;`,
    [id]);
};

module.exports = {
  connect,
  close,
  initDb,
  addTodo,
  allTodos,
  setCompleted
};
